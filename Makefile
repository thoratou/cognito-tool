REPO_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PUID=$(shell id -u)
PGID=$(shell id -g)

.PHONY: all
all: build run

.PHONY: build
build:
	@printf "\033[32m Local build\n\033[0m"
	@docker build \
		--build-arg PUID=$(PUID) \
		--build-arg PGID=$(PGID) \
		-t cognito-tool-local \
		-f local/Dockerfile .

.PHONY: run
run:
	@printf "\033[32m Run local\n\033[0m"
	@touch $(REPO_ROOT)/.bash_history_local
	@docker run -it --rm \
		--name cognito_tool_local \
		--env-file=.env \
		-v $$HOME/.ssh:/home/dev/.ssh \
		-v $$HOME/.gitconfig:/home/dev/.gitconfig \
		-v $(REPO_ROOT):/go/src/gitlab.com/thoratou/cognito-tool \
		-v $(REPO_ROOT)/.bash_history_local:/home/dev/.bash_history \
		cognito-tool-local
