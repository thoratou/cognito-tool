package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/thoratou/cognito-tool/jwtcognito"
)

var displayTokenCmd = &cobra.Command{
	Use:   "display-token",
	Short: "Display cache token information",
	Long:  `Display cache token information`,
	Run: func(cmd *cobra.Command, args []string) {
		accessTokenFlag, _ := cmd.Flags().GetBool("access-token")
		idTokenFlag, _ := cmd.Flags().GetBool("id-token")
		//refreshTokenFlag, _ := cmd.Flags().GetBool("refresh-token")
		//if !accessTokenFlag && !idTokenFlag && !refreshTokenFlag {
		if !accessTokenFlag && !idTokenFlag {
			//force to access token display if no option set
			accessTokenFlag = true
		}
		//TODO: find a way to display refresh token, as it is not JWT compliant
		//jwtcognito.DisplayToken(accessTokenFlag, idTokenFlag, refreshTokenFlag)
		jwtcognito.DisplayToken(accessTokenFlag, idTokenFlag, false)
	},
}

func init() {
	rootCmd.AddCommand(displayTokenCmd)
	displayTokenCmd.Flags().BoolP("access-token", "a", false, "Display access token information (default)")
	displayTokenCmd.Flags().BoolP("id-token", "i", false, "Display ID token information")
	//displayTokenCmd.Flags().BoolP("refresh-token", "r", false, "Display refresh token information")
}
