package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "cognito-tool",
	Short: "Tool to generate, verify and manage Cognito tokens using simple user/password login",
	Long: `Tool to generate, verify and manage Cognito tokens:
  * Connects using user and password
  * Stores generated access and refresh token
  * Automatically re-uses access token or refresh it
  * Displays inner information from the JWT access token
  * Validates the JWT access token using JWKS`,
}

//Execute entry point for cobra commands
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

func initConfig() {}
