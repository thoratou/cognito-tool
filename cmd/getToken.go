package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/thoratou/cognito-tool/cognito"
)

var getTokenCmd = &cobra.Command{
	Use:   "get-token",
	Short: "Retrieves the token from cache, if not existing or expired, generate it",
	Long:  `Retrieves the token from cache, if not existing or expired, generate it`,
	Run: func(cmd *cobra.Command, args []string) {
		force, _ := cmd.Flags().GetBool("force")
		noCache, _ := cmd.Flags().GetBool("no-cache")
		cognito.GetToken(force, noCache)
	},
}

func init() {
	rootCmd.AddCommand(getTokenCmd)
	getTokenCmd.Flags().BoolP("force", "f", false, "Force new access token generation")
	getTokenCmd.Flags().BoolP("no-cache", "n", false, "Do not retrieve from/store to cache")
}
