package cognito

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

//RenewWithRefreshToken uses current session refresh token to renew access token
func RenewWithRefreshToken(svc *cognitoidentityprovider.CognitoIdentityProvider, refreshToken string, clientID, userPoolID, region string) *cognitoidentityprovider.AdminInitiateAuthOutput {
	log.Println("--- refresh begin ---")
	params := &cognitoidentityprovider.AdminInitiateAuthInput{
		AuthFlow: aws.String("REFRESH_TOKEN_AUTH"),
		AuthParameters: map[string]*string{
			"REFRESH_TOKEN": aws.String(refreshToken),
		},
		ClientId:   aws.String(clientID),
		UserPoolId: aws.String(userPoolID),
	}

	resp, err := svc.AdminInitiateAuth(params)
	if err != nil {
		log.Println(err.Error())
		log.Println("--- refresh end ---")
		return nil
	}

	log.Println(resp)
	log.Println("--- refresh end ---")

	return resp
}
