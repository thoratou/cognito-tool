package cognito

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"

	mysession "gitlab.com/thoratou/cognito-tool/session"
	"gitlab.com/thoratou/cognito-tool/utils"
)

//GetToken retrieves toekn from cache. If notno chache or expired, create a new one
func GetToken(force bool, noCache bool) {
	username := utils.GetEnvVar("COGNITO_USER_NAME")
	password := utils.GetEnvVar("COGNITO_PASSWORD")
	clientID := utils.GetEnvVar("COGNITO_CLIENT_ID")
	userPoolID := utils.GetEnvVar("COGNITO_USER_POOL_ID")
	region := utils.GetEnvVar("COGNITO_REGION")
	cacheFolder := utils.GetEnvVar("COGNITO_TOOL_CACHE_FOLDER")

	svc := cognitoidentityprovider.New(
		session.New(),
		&aws.Config{
			Region:                        aws.String(region),
			MaxRetries:                    aws.Int(1),
			CredentialsChainVerboseErrors: aws.Bool(true),
			HTTPClient:                    &http.Client{Timeout: 10 * time.Second},
		})

	//get epoch time
	now := time.Now()
	secs := now.Unix()

	var finalSession *mysession.Session
	needCacheUpdate := false

	if force || noCache {
		log.Println("force-create new session")
		finalSession = newToken(svc, username, password, clientID, userPoolID, region, cacheFolder, secs)
		needCacheUpdate = true
	} else {
		mySession, err := mysession.NewSessionFromFile(cacheFolder)
		if err != nil {
			log.Println("cannot deserialize session from cache: " + err.Error())
			log.Println("create new session instead")
			finalSession = newToken(svc, username, password, clientID, userPoolID, region, cacheFolder, secs)
			needCacheUpdate = true
		} else if mySession.IsExpired(secs) {
			finalSession = renewToken(svc, mySession, username, password, clientID, userPoolID, region, cacheFolder, secs)
			if finalSession == nil {
				//session cannot be renewed -> start new session
				finalSession = newToken(svc, username, password, clientID, userPoolID, region, cacheFolder, secs)
			}
			needCacheUpdate = true
		} else {
			log.Println("session still valid, reuse it")
			finalSession = mySession
		}
	}

	log.Println(finalSession)
	fmt.Println(finalSession.AccessToken)

	if needCacheUpdate && !noCache {
		finalSession.SerializeToFile(cacheFolder)
	}

}

func newToken(svc *cognitoidentityprovider.CognitoIdentityProvider,
	username, password, clientID, userPoolID, region, cacheFolder string,
	secs int64) *mysession.Session {
	resp := Login(
		svc,
		username,
		password,
		clientID,
		userPoolID,
		region,
	)

	cognitoResults := RenewPasswordIfRequired(
		svc,
		resp,
		username,
		password,
		clientID,
		userPoolID,
		region,
	)

	mySession := mysession.NewSessionFromCognitoResults(cognitoResults)
	mySession.AddExpires(secs, *cognitoResults.ExpiresIn)

	return mySession
}

func renewToken(svc *cognitoidentityprovider.CognitoIdentityProvider,
	previousSession *mysession.Session,
	username, password, clientID, userPoolID, region, cacheFolder string,
	secs int64) *mysession.Session {
	log.Println("session expired, use refresh token to renew it")
	resp := RenewWithRefreshToken(
		svc,
		previousSession.RefreshToken,
		clientID,
		userPoolID,
		region,
	)

	if resp == nil {
		//Refresh token expired too at this point
		return nil
	}

	cognitoResults := RenewPasswordIfRequired(
		svc,
		resp,
		username,
		password,
		clientID,
		userPoolID,
		region,
	)

	mySession := mysession.NewSessionFromCognitoResultsAndPreviousSession(cognitoResults, previousSession)
	mySession.AddExpires(secs, *cognitoResults.ExpiresIn)

	return mySession
}
