package cognito

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

//Login login using username and password on Cognito
func Login(svc *cognitoidentityprovider.CognitoIdentityProvider, username, password, clientID, userPoolID, region string) *cognitoidentityprovider.AdminInitiateAuthOutput {
	log.Println("--- authentication begin ---")
	params := &cognitoidentityprovider.AdminInitiateAuthInput{
		AuthFlow: aws.String("ADMIN_NO_SRP_AUTH"),
		AuthParameters: map[string]*string{
			"USERNAME": aws.String(username),
			"PASSWORD": aws.String(password),
		},
		ClientId:   aws.String(clientID),
		UserPoolId: aws.String(userPoolID),
	}

	resp, err := svc.AdminInitiateAuth(params)
	if err != nil {
		log.Println(err.Error())
		return nil
	}

	log.Println(resp)
	log.Println("--- authentication end ---")

	return resp
}
