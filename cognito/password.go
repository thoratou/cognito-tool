package cognito

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

//RenewPasswordIfRequired if challenged, automatical password renew on Cognito
func RenewPasswordIfRequired(svc *cognitoidentityprovider.CognitoIdentityProvider, resp *cognitoidentityprovider.AdminInitiateAuthOutput, username, password, clientID, userPoolID, region string) *cognitoidentityprovider.AuthenticationResultType {
	if resp.ChallengeName != nil && *resp.ChallengeName == "NEW_PASSWORD_REQUIRED" {
		fmt.Println("--- new password begin ---")
		session := resp.Session
		params := &cognitoidentityprovider.AdminRespondToAuthChallengeInput{
			ChallengeName: aws.String("NEW_PASSWORD_REQUIRED"),
			ChallengeResponses: map[string]*string{
				"NEW_PASSWORD": aws.String(password),
				"USERNAME":     aws.String(username),
			},
			ClientId:   aws.String(clientID),
			Session:    session,
			UserPoolId: aws.String(userPoolID),
		}

		resp, err := svc.AdminRespondToAuthChallenge(params)
		if err != nil {
			fmt.Println(err.Error())
			return nil
		}
		fmt.Println(resp)
		fmt.Println("--- new password end ---")
		return resp.AuthenticationResult
	}
	return resp.AuthenticationResult
}
