package jwtcognito

import (
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
)

//ParseAndValidateToken parse and validate toekn string into validated token structure
func ParseAndValidateToken(tokenString *string, jwkKeySet jwk.Set) (jwt.Token, error) {
	//JWT validation and extract
	token, err := jwt.Parse([]byte(*tokenString), jwt.WithKeySet(jwkKeySet))
	return token, err
}
