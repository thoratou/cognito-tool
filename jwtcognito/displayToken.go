package jwtcognito

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws/awsutil"
	"gitlab.com/thoratou/cognito-tool/jwkscache"
	"gitlab.com/thoratou/cognito-tool/session"
	"gitlab.com/thoratou/cognito-tool/utils"
)

//DisplayToken displays information on the current cached session
func DisplayToken(accessTokenFlag, idTokenFlag, refreshTokenFlag bool) {
	cacheFolder := utils.GetEnvVar("COGNITO_TOOL_CACHE_FOLDER")
	mySession, err := session.NewSessionFromFile(cacheFolder)
	if err != nil {
		fmt.Println("cannot deserialize session from cache: " + err.Error())
	} else {

		jwksCache, err := jwkscache.GetTokenFromCacheFile()

		if err != nil {
			fmt.Println("cannot retrieve jwks from cache: " + err.Error())
			return
		}

		if accessTokenFlag {
			fmt.Println("-- Access token --")
			displayTokenFromString(&mySession.AccessToken, jwksCache)
		}

		if idTokenFlag {
			fmt.Println("-- ID token --")
			displayTokenFromString(&mySession.IDToken, jwksCache)
		}

		if refreshTokenFlag {
			fmt.Println("-- Refresh token --")
			displayTokenFromString(&mySession.RefreshToken, jwksCache)
		}
	}
}

func displayTokenFromString(tokenString *string, jwksCache *jwkscache.JwksCache) {
	token, err := ParseAndValidateToken(tokenString, jwksCache.Jwks)

	if err != nil {
		fmt.Println("invalid token: " + err.Error())
	}

	if token != nil {
		fmt.Println(awsutil.Prettify(token.PrivateClaims()))
	}
}
