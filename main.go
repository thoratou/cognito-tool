package main

import (
	"log"
	"os"

	"gitlab.com/thoratou/cognito-tool/cmd"
	"gitlab.com/thoratou/cognito-tool/utils"
)

func setLogOutput(folder string) *os.File {
	//create folder if not exist
	err := os.MkdirAll(folder, 0755)
	if err != nil {
		log.Fatalf("Create directories error: %v\n", err)
	}

	logFile, err := os.OpenFile(folder+"/cognito-tool.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file: %v\n", err)
	}
	return logFile
}

func main() {
	cacheFolder := utils.GetEnvVar("COGNITO_TOOL_CACHE_FOLDER")

	logFile := setLogOutput(cacheFolder)
	defer logFile.Close()

	log.SetOutput(logFile)

	cmd.Execute()
}
