module gitlab.com/thoratou/cognito-tool

go 1.15

require (
	github.com/aws/aws-sdk-go v1.37.6
	github.com/lestrrat-go/jwx v1.1.1
	github.com/spf13/cobra v1.1.1
)
