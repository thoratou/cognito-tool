package session

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

//Session current current after cognito authentication
type Session struct {
	AccessToken  string `json:"accessToken"`
	IDToken      string `json:"idToken"`
	RefreshToken string `json:"refreshToken"`
	ExpiresAt    int64  `json:"expiresAt"`
}

//NewSessionFromCognitoResults creates a session handler from cognito authentication results
func NewSessionFromCognitoResults(cognitoResults *cognitoidentityprovider.AuthenticationResultType) *Session {
	return &Session{
		AccessToken:  *cognitoResults.AccessToken,
		IDToken:      *cognitoResults.IdToken,
		RefreshToken: *cognitoResults.RefreshToken,
		ExpiresAt:    0,
	}
}

//NewSessionFromCognitoResultsAndPreviousSession creates a session handler from merge view of cognito authentication results and previous session
func NewSessionFromCognitoResultsAndPreviousSession(cognitoResults *cognitoidentityprovider.AuthenticationResultType, previousSession *Session) *Session {
	return &Session{
		AccessToken:  *cognitoResults.AccessToken,
		IDToken:      *cognitoResults.IdToken,
		RefreshToken: previousSession.RefreshToken,
		ExpiresAt:    0,
	}
}

// AddExpires add expire time to current session
func (s *Session) AddExpires(currentEpoch int64, expiresIn int64) {
	s.ExpiresAt = currentEpoch + expiresIn
}

// IsExpired check if session is expired
func (s *Session) IsExpired(currentEpoch int64) bool {
	return currentEpoch >= s.ExpiresAt
}

// String returns the string representation
func (s *Session) String() string {
	return awsutil.Prettify(s)
}

// SerializeToFile serializes session content to file
func (s *Session) SerializeToFile(folder string) {
	//create folder if not exist
	err := os.MkdirAll(folder, 0755)
	if err != nil {
		log.Printf("Create directories error: %v\n", err)
		panic(err)
	}

	buffer, err := json.Marshal(s)
	if err != nil {
		log.Printf("Cannot serialise session content: %v\n", err)
		panic(err)
	}

	file, err := os.Create(folder + "/session.cache")
	if err != nil {
		log.Printf("Cannot create "+folder+"/session.cache file: %v\n", err)
		panic(err)
	}

	defer file.Close()

	_, err = file.Write(buffer)
	if err != nil {
		log.Printf("Cannot write into "+folder+"/session.cache file: %v\n", err)
		panic(err)
	}

	file.Sync()
}

// NewSessionFromFile deserializes session content from file
func NewSessionFromFile(folder string) (*Session, error) {

	file, err := os.Open(folder + "/session.cache")
	if err != nil {
		return nil, err
	}

	defer file.Close()

	buffer, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	session := &Session{}

	err = json.Unmarshal(buffer, session)
	if err != nil {
		return nil, err
	}

	return session, nil
}
