package jwkscache

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/lestrrat-go/jwx/jwk"
	"gitlab.com/thoratou/cognito-tool/utils"
)

//GetTokenFromCacheFile retrieve the jwk cache from file and renew it if required
func GetTokenFromCacheFile() (*JwksCache, error) {
	cacheFolder := utils.GetEnvVar("COGNITO_TOOL_CACHE_FOLDER")
	needNewJwksCache := false
	//get epoch time
	now := time.Now()
	secs := now.Unix()

	jwksCache, err := NewJwksCacheFromFile(cacheFolder)
	if err != nil {
		log.Println("cannot read jwks from cache: " + err.Error())
		log.Println("create jwks and store it")
		needNewJwksCache = true
	} else if jwksCache.IsExpired(secs) {
		log.Println("jwks cache expired, need to create new one")
		needNewJwksCache = true
	}

	if needNewJwksCache {
		jwksCache, err = newJwksCache()

		if err != nil {
			fmt.Println("cannot retrieve JWKS: " + err.Error())
			return nil, err
		}

		jwksCache.SerializeToFile(cacheFolder)
	}
	return jwksCache, nil
}

var globalJWKSMutex sync.Mutex
var globalJWKSCache *JwksCache

//GetTokenFromGlobalInstance retrieve the jwk cache global instance and renew it if required
func GetTokenFromGlobalInstance() (*JwksCache, error) {
	//get epoch time
	now := time.Now()
	secs := now.Unix()

	var err error
	if globalJWKSCache == nil {
		globalJWKSMutex.Lock()
		defer globalJWKSMutex.Unlock()
		if globalJWKSCache == nil {
			log.Println("no existing jwks cache instance")
			log.Println("create new instance")

			globalJWKSCache, err = newJwksCache()

			if err != nil {
				fmt.Println("cannot return JWKS: " + err.Error())
				return nil, err
			}
		}
	} else if globalJWKSCache.IsExpired(secs) {
		globalJWKSMutex.Lock()
		defer globalJWKSMutex.Unlock()
		if globalJWKSCache.IsExpired(secs) {
			log.Println("jwks cache expired, need to create new one")
			globalJWKSCache, err = newJwksCache()

			if err != nil {
				fmt.Println("cannot return JWKS: " + err.Error())
				return nil, err
			}
		}
	}

	return globalJWKSCache, nil
}

func newJwksCache() (*JwksCache, error) {
	log.Println("--- jwks cache creation begin ---")

	userPoolID := utils.GetEnvVar("COGNITO_USER_POOL_ID")
	region := utils.GetEnvVar("COGNITO_REGION")
	jwksURL := fmt.Sprintf("https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json",
		region, userPoolID)

	ctx := context.Background()
	keySet, err := jwk.Fetch(ctx, jwksURL)
	if err != nil {
		log.Println("cannot return JWKS: " + err.Error())
		return nil, err
	}

	jwksCache := NewJwksCacheFromKeySet(keySet)

	//get epoch time
	now := time.Now()
	secs := now.Unix()

	jwkCacheExpiracyDelay := utils.GetEnvVarAsInt64("JWKS_CACHE_EXPIRACY_DELAY")
	jwksCache.AddExpires(secs, jwkCacheExpiracyDelay)
	log.Println("--- jwks cache creation end ---")

	return jwksCache, nil
}
