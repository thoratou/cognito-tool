package jwkscache

import (
	"os"
	"sync"
	"testing"
	"time"
)

func TestGlobalCache(t *testing.T) {
	//change JWKS expiracy
	delay := os.Getenv("JWKS_CACHE_EXPIRACY_DELAY")
	os.Setenv("JWKS_CACHE_EXPIRACY_DELAY", "2")

	var cache1 *JwksCache
	var cache2 *JwksCache
	var err1 error
	var err2 error

	var wg sync.WaitGroup

	//init mutex test
	wg.Add(2)

	go func() {
		cache1, err1 = GetTokenFromGlobalInstance()
		wg.Done()
	}()

	go func() {
		cache2, err2 = GetTokenFromGlobalInstance()
		wg.Done()
	}()

	wg.Wait()

	if err1 != nil {
		t.Fatal("err1:" + err1.Error())
	}

	if err2 != nil {
		t.Fatal("err2:" + err2.Error())
	}

	if cache1 != cache2 {
		t.Fatal("different cache instances returned")
	}

	//expiracy test

	time.Sleep(3 * time.Second)

	cache3, err3 := GetTokenFromGlobalInstance()

	if err3 != nil {
		t.Fatal("err3:" + err3.Error())
	}

	if cache3 == cache1 {
		t.Fatal("different cache instances expected")
	}

	if cache3.ExpiresAt <= cache1.ExpiresAt {
		t.Fatal("expected different cache expiracy")
	}

	//expiracy mutex test

	time.Sleep(2 * time.Second)

	wg.Add(2)

	go func() {
		cache1, err1 = GetTokenFromGlobalInstance()
		wg.Done()
	}()

	go func() {
		cache2, err2 = GetTokenFromGlobalInstance()
		wg.Done()
	}()

	wg.Wait()

	if err1 != nil {
		t.Fatal("err1:" + err1.Error())
	}

	if err2 != nil {
		t.Fatal("err2:" + err2.Error())
	}

	if cache1 != cache2 {
		t.Fatal("different cache instances returned")
	}

	//restore JWKS expiracy
	os.Setenv("JWKS_CACHE_EXPIRACY_DELAY", delay)
}
