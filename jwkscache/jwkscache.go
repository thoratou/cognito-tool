package jwkscache

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/lestrrat-go/jwx/jwk"

	"github.com/aws/aws-sdk-go/aws/awsutil"
)

//JwksCache local jwks cache data with expiracy handling
type JwksCache struct {
	Jwks      jwk.Set `json:"jwks"`
	ExpiresAt int64   `json:"expiresAt"`
}

// NewJwksCacheFromKeySet create new cache instance from JWK key set
func NewJwksCacheFromKeySet(set jwk.Set) *JwksCache {
	return &JwksCache{
		Jwks:      set,
		ExpiresAt: 0,
	}
}

// AddExpires add expire time to current jwk cache
func (j *JwksCache) AddExpires(currentEpoch int64, expiresIn int64) {
	j.ExpiresAt = currentEpoch + expiresIn
}

// IsExpired check if jwks cache is expired
func (j *JwksCache) IsExpired(currentEpoch int64) bool {
	return currentEpoch >= j.ExpiresAt
}

// String returns the string representation
func (j *JwksCache) String() string {
	return awsutil.Prettify(j)
}

// SerializeToFile serializes jwks cache content to file
func (j *JwksCache) SerializeToFile(folder string) {
	//create folder if not exist
	err := os.MkdirAll(folder, 0755)
	if err != nil {
		log.Printf("Create directories error: %v\n", err)
		panic(err)
	}

	buffer, err := json.Marshal(j)
	if err != nil {
		log.Printf("Cannot serialise jwks cache content: %v\n", err)
		panic(err)
	}

	file, err := os.Create(folder + "/jwks.cache")
	if err != nil {
		log.Printf("Cannot create "+folder+"/jwks.cache file: %v\n", err)
		panic(err)
	}

	defer file.Close()

	_, err = file.Write(buffer)
	if err != nil {
		log.Printf("Cannot write into "+folder+"/jwks.cache file: %v\n", err)
		panic(err)
	}

	file.Sync()
}

// NewJwksCacheFromFile deserializes jwks cache content from file
func NewJwksCacheFromFile(folder string) (*JwksCache, error) {

	file, err := os.Open(folder + "/jwks.cache")
	if err != nil {
		return nil, err
	}

	defer file.Close()

	buffer, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	jwkscache := &JwksCache{}

	err = json.Unmarshal(buffer, jwkscache)
	if err != nil {
		return nil, err
	}

	return jwkscache, nil
}
