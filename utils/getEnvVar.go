package utils

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

//GetEnvVar returns environment variable as string
func GetEnvVar(varname string) string {
	value := os.Getenv(varname)
	if value == "" {
		fmt.Printf("Missing environment variable: %v\n", varname)
		log.Fatalf("Missing environment variable: %v", varname)
	}
	return value
}

//GetEnvVarAsInt64 returns environment variable as int64
func GetEnvVarAsInt64(varname string) int64 {
	value := GetEnvVar(varname)
	value64, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		fmt.Printf("Cannot convert environment %v variable with value %v to int64: %v\n", varname, value, err)
		log.Fatalf("Cannot convert environment %v variable with value %v to int64: %v", varname, value, err)
	}
	return value64
}
